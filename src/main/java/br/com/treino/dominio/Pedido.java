package br.com.treino.dominio;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Cliente cliente;
    @Column(nullable = false)
    private LocalDate dataPedido;
    @Column(nullable = false)
    private BigDecimal total;
}
